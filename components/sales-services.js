import React from "react";
import css from "./sales-services.module.css";
import Link from "next/link";

export default function services(props) {
  let params = props.params.split(" ");

  let code = [];
  for(let module of params){
    let link = "null";
    let imgSrc = "/components/sales-services/" + module + ".svg";
    code.push(<div className={css.module}>
      <img className={css.module_img} src={imgSrc}/>
      <h1 className={css.module_miniH1}>{module.charAt(0).toUpperCase() + module.slice(1)}</h1>
      <Link href={link}><a className={css.module_a}>Learn more</a></Link>
    </div>);
  }
  return(
    <section className={css.salesServices}>
      <h3 className={css.salesServices_h3}>OUR SALESFORCE SERVICES</h3>
      <h1 className={css.salesServices_h1}>Looking for another Salesforce service?</h1>
      <h2 className={css.salesServices_h2}>ForceComprehensive’s team provides variety of other Salesforce solutions and services which could be suitable for your organization.</h2>
      <div className={css.salesServices_grid}>
        {code}
      </div>
    </section>
  )
}