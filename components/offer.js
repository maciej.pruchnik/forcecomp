import React from "react";
      import css from "../components/offer.module.css";
      import Link from "next/link";

      export default function offer(props) {
      return (
      <section className={css.offer}>
      <h3 className={css.offer_h3}>LETS COLLABORATE</h3>
      <h1 className={css.offer_h1}>{props.h1}</h1>
      <Link href={props.link}><a className={css.offer_a}>{props.a}</a></Link>
      <img className={css.offer_svg1} src="/components/offer/svg1.svg"/>
      <img className={css.offer_svg2} src="/components/offer/svg2.svg"/>
      <img className={css.offer_svg3} src="/components/offer/svg3.svg"/>
    </section>
  )
}
