import React from "react";
import css from "./sales-admin.module.css";
import component from "../components/css-components.module.css";
import Layout from "../components/layout.js";
import SalesServices from "../components/sales-services.js";
import Offer from "../components/offer.js";

export default function index() {
  return(
    <Layout>
      <section className={css.empower}>
        <h1 className={css.empower_h1}>How does our Salesforce administration service empower you?</h1>
        <h2 className={css.empower_h2}>Our talented team of Salesforce admin experts will empower and elevate your business and your Salesforce experience in the following ways:</h2>
        <div className={css.empower_grid}>
          <img src="/sales-admin/empower/shield.svg"/>
          <img src="/general/money.svg"/>
          <img src="/sales-admin/lock.svg"/>
          <img src="/sales-admin/empower/key.svg"/>
          <div className={css.empower_divider}/>
          <div className={css.empower_divider}/>
          <div className={css.empower_divider}/>
          <div className={css.empower_divider}/>
          <h1 className={css.empower_miniH1}>Reliability</h1>
          <h1 className={css.empower_miniH1}>Evolve and future-proof your investment</h1>
          <h1 className={css.empower_miniH1}>Avoid unwanted commitments or investments</h1>
          <h1 className={css.empower_miniH1}>Adherence to data governance principles</h1>
          <h2 className={css.empower_miniH2}>We ensure your Salesforce implementation is always working at peak performance.</h2>
          <h2 className={css.empower_miniH2}>We ensure your Salesforce implementation keeps up with your evolving business needs.</h2>
          <h2 className={css.empower_miniH2}>Protect your budget and ensure Salesforce-related costs are predictable.</h2>
          <h2 className={css.empower_miniH2}>We’ll help to ensure data quality, data security, data ownership, and data access needs, are observed.</h2>

          <img src="/sales-admin/empower/clipboard.svg"/>
          <img src="/sales-admin/empower/settings.svg"/>
          <img src="/sales-admin/empower/message-square.svg"/>
          <img src="/sales-admin/empower/bookOpen.svg"/>
          <div className={css.empower_divider}/>
          <div className={css.empower_divider}/>
          <div className={css.empower_divider}/>
          <div className={css.empower_divider}/>
          <h1 className={css.empower_miniH1}>Reporting</h1>
          <h1 className={css.empower_miniH1}>Integration and customization support</h1>
          <h1 className={css.empower_miniH1}>End-consumer support</h1>
          <h1 className={css.empower_miniH1}>Education and better service delivery</h1>
          <h2 className={css.empower_miniH2}>Keep abreast of our Salesforce service and your performance through regular professional reports.</h2>
          <h2 className={css.empower_miniH2}>We ensure that your digital ecosystem and Salesforce solutions work seamlessly, conveniently, and intuitively.</h2>
          <h2 className={css.empower_miniH2}>We work with your IT team or directly with your end-consumer to ensure your Salesforce-related services and queries are rendered as expected.</h2>
          <h2 className={css.empower_miniH2}>We educate both end-users and staff to ensure smooth product delivery, happy consumers, and smarter business practice.</h2>
        </div>
      </section>

      <section className={css.scope}>
        <h1 className={component.list_h1}>Scope of our administration service</h1>
        <h2 className={component.list_h2}>ForceComprehensive wants your business to fully benefit from your Salesforce investment. Our certified Salesforce admins will work with you in the following ways:</h2>
        <div className={component.list_grid}>
          <div className={component.list_container}>
            <h3 className={component.list_h3}>1</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <div className={component.list_container}>
            <h3 className={component.list_h3}>2</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <div className={component.list_container}>
            <h3 className={component.list_h3}>3</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <h1 className={component.list_miniH1}>Salesforce functionality</h1>
          <h1 className={component.list_miniH1}>Business architecture design</h1>
          <h1 className={component.list_miniH1}>Business process automation</h1>
          <h2 className={component.list_miniH2}>We manage typical Salesforce functionality for your business including, Service Cloud, Marketing Cloud, Sales Cloud, eCommerce Cloud, Heroku Cloud Service/Mobile App engagement, Enterprise cloud, MuleSoft Anypoint Platform integrations, Lightning Platform, and Community Cloud.</h2>
          <h2 className={component.list_miniH2}>We help to design and uncover a suitable business architecture that will tap Salesforce’s full potential in supporting your business needs.</h2>
          <h2 className={component.list_miniH2}>We use Salesforce’s point-and-click features to automate business processes such as assigning tasks, automatic email alerts, notifications, etc.</h2>

          <div className={component.list_container}>
            <h3 className={component.list_h3}>4</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <div className={component.list_container}>
            <h3 className={component.list_h3}>5</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <div className={component.list_container}>
            <h3 className={component.list_h3}>6</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <h1 className={component.list_miniH1}>User profile management</h1>
          <h1 className={component.list_miniH1}>Data security management</h1>
          <h1 className={component.list_miniH1}>Regular reporting</h1>
          <h2 className={component.list_miniH2}>Salesforce user-profiles define what a user can do based on various permissions. Roles define what a user has access to based on their hierarchy. For example, sales-reps will only want to see data that is relevant to their sales work. A sales manager will need more in-depth data about all sales reps and can do more with the data they have access to. We also add and delete user accounts as required.</h2>
          <h2 className={component.list_miniH2}>We ensure that your access to Salesforce data, applications, and solutions is never compromised. If we uncover any potential threats, we get rid of them immediately.</h2>
          <h2 className={component.list_miniH2}>We make sure that you remain informed by letting you know what matters to you the most. Our experts can also create custom fields to ensure that the data that you receive is customized to your specific needs. A reporting schedule can appropriately be agreed upon for status and security health reports.</h2>

          <div className={component.list_container}>
            <h3 className={component.list_h3}>7</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <div className={component.list_container}>
            <h3 className={component.list_h3}>8</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <div className={component.list_container}>
            <h3 className={component.list_h3}>9</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <h1 className={component.list_miniH1}>Salesforce configuration and optimization</h1>
          <h1 className={component.list_miniH1}>End-user training</h1>
          <h1 className={component.list_miniH1}>User support</h1>
          <h2 className={component.list_miniH2}>We configure and optimize Salesforce validation rules, workflows, tabs, AppExchange apps, dashboards, reports, etc., to ensure that they match business logic, UI, and data model requirements of your Salesforce solution.</h2>
          <h2 className={component.list_miniH2}>We provide training to staff and/or end-users to ensure smooth product delivery, happy consumers, and smarter business practice.</h2>
          <h2 className={component.list_miniH2}>We provide user support to ensure that users get the most out of Salesforce and that they can resolve any issues they come across expeditiously through our help desk.</h2>

          <div className={component.list_container}>
            <h3 className={component.list_h3}>10</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <div className={component.list_container}>
            <h3 className={component.list_h3}>11</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <div className={component.list_container}>
            <h3 className={component.list_h3}>12</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <h1 className={component.list_miniH1}>Salesforce monitoring and performance checks</h1>
          <h1 className={component.list_miniH1}>SLA-based improvements and resolutions</h1>
          <h1 className={component.list_miniH1}>Third-party apps and integrations</h1>
          <h2 className={component.list_miniH2}>We monitor vital parameters to ensure the smooth operation of your Salesforce solutions. For example, we can monitor logs and workloads to ensure that everything is running efficiently and that there are no slow-downs or malfunctions.</h2>
          <h2 className={component.list_miniH2}>Improvements and resolutions are undertaken according to an agreed bucket of hours in Service Level Agreements (SLAs). These improvements and resolutions are typically beyond the scope of Salesforce administration, but we sort them out to ensure a smooth workflow without having to disrupt your budget or operations.</h2>
          <h2 className={component.list_miniH2}>We support and manage third-party apps like HelloSign for Salesforce, Formstack, Trail Tracker, etc. and SaaS Salesforce integrations like HubSpot, DocuSign, SAP, SharePoint, QuickBooks, etc.</h2>
        </div>
      </section>

      <section className={css.expect}>
        <h3 className={component.imgList_h3}>ENGAGEMENT MODEL</h3>
        <h1 className={component.imgList_h1}>What to expect from our Salesforce administration service</h1>
        <h2 className={component.imgList_h2}>Regardless of whether we (or another third-party) have provided other Salesforce services to you or not, we use the following basic guideline to determine and recommend the kind of Salesforce admin service you need:</h2>
        <div className={component.imgList_grid}>
          <div className={component.imgList_container}>
            <img className={component.imgList_img} src="/sales-admin/expect/search.svg"/>
            <img className={component.imgList_imgBack} src="/general/circleWhite.svg"/>
          </div>
          <div className={component.imgList_container}>
            <img className={component.imgList_img} src="/sales-admin/expect/monitor.svg"/>
            <img className={component.imgList_imgBack} src="/general/circleWhite.svg"/>
          </div>
          <div className={component.imgList_container}>
            <img className={css.expect_imgSpecial} src="/sales-admin/expect/performance.svg"/>
            <img className={component.imgList_imgBack} src="/general/circleWhite.svg"/>
          </div>
          <div className={component.imgList_container}>
            <img className={component.imgList_img} src="/sales-admin/lock.svg"/>
            <img className={component.imgList_imgBack} src="/general/circleWhite.svg"/>
          </div>
          <h1 className={component.imgList_microH1}>Solution health-check</h1>
          <h1 className={component.imgList_microH1}>Tests and reviews</h1>
          <h1 className={component.imgList_microH1}>Performance analysis</h1>
          <h1 className={component.imgList_microH1}>Security audit</h1>
          <h2 className={component.imgList_miniH2}>We perform a comprehensive health-check of your Salesforce solution.</h2>
          <h2 className={component.imgList_miniH2}>We test and review your current integrations and customizations.</h2>
          <h2 className={component.imgList_miniH2}>We conduct a performance analysis of different components of your Salesforce solution including custom apps.</h2>
          <h2 className={component.imgList_miniH2}>We conduct a performance analysis of different components of your Salesforce solution including custom apps.</h2>
          <div className={component.imgList_container}>
            <img className={component.imgList_img} src="/sales-admin/expect/blocks.svg"/>
            <img className={component.imgList_imgBack} src="/general/circleWhite.svg"/>
          </div>
          <div/><div/><div/>
          <h1 className={component.imgList_microH1}>Options to choose</h1>
          <h1/><h1/><h1/>
          <h2 className={component.imgList_miniH2}>We provide you with options to choose for our service based on your available resource capacity (personnel, finances, infrastructure, etc.).</h2>
          <h2/><h2/><h2/>
        </div>
      </section>

      <section className={css.costing}>
        <div>
          <h3 className={css.costing_h3}>COSTING</h3>
          <h2 className={css.costing_h1}>Some of the primary considerations for the costing of our service include:</h2>
          <div className={css.costing_divider}/>
        </div>
        <div>
          <div className={css.costing_container}>
            <h1 className={css.costing_miniH1}>1</h1>
            <h2 className={css.costing_miniH2}>SLA agreement and the plan option you choose.</h2>
          </div>
          <div className={css.costing_container}>
            <h1 className={css.costing_miniH1}>2</h1>
            <h2 className={css.costing_miniH2}>Size of the business and the number of Salesforce users.</h2>
          </div>
          <div className={css.costing_container}>
            <h1 className={css.costing_miniH1}>3</h1>
            <h2 className={css.costing_miniH2}>The complexity of Salesforce integrations and customizations.</h2>
          </div>
        </div>
      </section>

      <SalesServices params="consulting integration customization extension"/>

      <Offer h1="Expect only the best from our Salesforce admin service. We will be glad to provide you with any additional information and answer your queries."
             link="null"
             a="Talk to us"/>
    </Layout>
  )
}