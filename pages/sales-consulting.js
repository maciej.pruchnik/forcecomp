import React from "react";
import css from "./sales-consulting.module.css";
import component from "../components/css-components.module.css";
import Layout from "../components/layout.js";
import SalesServices from "../components/sales-services.js";
import Offer from "../components/offer.js";

export default function consulting(){
  return(
    <Layout>
      <section className={css.challenges}>
        <div>
          <h1 className={css.challenges_h1}>Business challenges we solve</h1>
          <div className={css.challenges_divider}/>
          <h2 className={css.challenges_h2}>We offer in-depth, functional expertise, and a holistic perspective to squeeze out every ounce of value that can be captured across geographic boundaries or between silos in any organization.</h2>
          <h2 className={css.challenges_h2}>Some of the challenges that our clients approach us to help them solve using Salesforce include:</h2>
        </div>
        <div className={css.challenges_solutions}>

        </div>
      </section>

      <section className={css.do}>
        <div className={css.do_left}>
          <h3 className={css.do_h3}>What We Do?</h3>
          <h1 className={css.do_h1}>We offer new ideas, research-proven methods, and specialized knowledge to simplify your Salesforce journey.</h1>
          <div className={css.do_divider}/>
          <h2 className={css.do_h2}>Ultimately, we help your business reach, acquire, convert, and retain more customers by keeping them happy through tightly integrating your company, stakeholders, and systems with robust and faster app integration. Our end-to-end Salesforce experts do this by:</h2>
          <a className={css.do_button} href="null">Request a call</a>
        </div>
        <div className={css.do_right}>
          <div className={css.do_container}>
            <h1 className={css.do_num}>01</h1>
            <h1 className={css.do_macroH1}>Implementing reliable cutting-edge solutions</h1>
            <h2 className={css.do_miniH2}>We analyze our client’s requirements and offer an initial evaluation, including ways to fulfill business needs using Salesforce’s full capabilities to solve critical issues and uncover opportunities. Client requirements range from digitizing, customizing, and migrating from other CRMs to Salesforce, to improving siloed services (e.g., sales, marketing, and customer service) into an integrated CRM platform.</h2>
          </div>
          <div className={css.do_container}>
            <h1 className={css.do_num}>02</h1>
            <h1 className={css.do_macroH1}>Upgrading existing solutions</h1>
            <h2 className={css.do_miniH2}>We partner with our clients to deliver robust, actionable strategies for enhancing current Salesforce solutions with suitable customizations and integrations that fit client needs. Enhancements can range from customizing with code to modifying with point-and-click tools.</h2>
          </div>
        </div>
      </section>

      <section className={css.services}>
        <h1 className={component.list_h1}>ForceComprehensive’s Salesforce consulting services</h1>
        <h2 className={component.list_h2}>Whether you plan to implement a new Salesforce solution (or to upgrade an existing one), ForceComprehensive offers full Salesforce consulting services that include:</h2>
        <div className={component.list_grid}>
          <div className={component.list_container}>
            <h3 className={component.list_h3}>1</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <div className={component.list_container}>
            <h3 className={component.list_h3}>2</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <div className={component.list_container}>
            <h3 className={component.list_h3}>3</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <h1 className={component.list_miniH1}>Delineating and mapping business objectives</h1>
          <h1 className={component.list_miniH1}>Addressing critical issues and uncovering opportunities</h1>
          <h1 className={component.list_miniH1}>Recommending suitable Salesforce tools</h1>
          <h2 className={component.list_miniH2}>We work with clients to outline clear business objectives that Salesforce can support and then map these objectives into Salesforce features that can be achieved.</h2>
          <h2 className={component.list_miniH2}>We help clients address challenges in an efficient and timely manner, whether it involves reducing case resolution time, introducing efficient email marketing, automating sales processes, increasing visibility in reports, etc.</h2>
          <h2 className={component.list_miniH2}>We help clients choose and only adopt what they need and want to use.</h2>

          <div className={component.list_container}>
            <h3 className={component.list_h3}>4</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <div className={component.list_container}>
            <h3 className={component.list_h3}>5</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <div className={component.list_container}>
            <h3 className={component.list_h3}>6</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <h1 className={component.list_miniH1}>Plan and manage Salesforce customization and integration</h1>
          <h1 className={component.list_miniH1}>Seamless Salesforce implementation</h1>
          <h1 className={component.list_miniH1}>Salesforce enablement</h1>
          <h2 className={component.list_miniH2}>We help define necessary customizations that are aligned with business processes and map any necessary integrations with external systems to ensure maximum utilization and full compliance.</h2>
          <h2 className={component.list_miniH2}>We help clients strategize and implement Salesforce within scope, budget, and schedule to address business needs. Our Salesforce consultants supervise the Salesforce implementation process to ensure that our clients’ needs are fully realized.</h2>
          <h2 className={component.list_miniH2}>We design a user adoption strategy for our clients to reduce the learning curve and quickly master the new system. Enablement can include iterative user training, release notes, and responsive user support.</h2>

          <div className={component.list_container}>
            <h3 className={component.list_h3}>7</h3>
            <img className={component.list_img} src="/general/circleLightBlue.svg"/>
          </div>
          <div/><div/>
          <h1 className={component.list_miniH1}>Process support within the Salesforce ecosystem</h1>
          <div/><div/>
          <h2 className={component.list_miniH2}>Our Salesforce consultants do this by gathering requirements and collaborating with the client in designing and implementing Salesforce solutions. Our experts also provide comprehensive Salesforce support to ensure maximum effectiveness of implemented solutions.</h2>
        </div>
        <img className={css.services_img} src="/sales-consulting/services/svg1.svg"/>
      </section>


      <section className={css.products}>

      </section>

      <SalesServices params="extension integration customization administration"/>
      <Offer h1="You understand your business better than anyone else. Let us help you thrive even more in your competitive environment."
                     a="Reach a consultant"
                     link="null"/>
    </Layout>
  )
}