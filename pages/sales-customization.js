import React from "react"
import css from "./sales-customization.module.css";
import Layout from "../components/layout.js";
import SalesServices from "../components/sales-services.js";
import Offer from "../components/offer.js";

export default function customization() {
  return (
    <Layout>
      <section className={css.experience}>

      </section>

      <section className={css.adapt}>

      </section>

      <section className={css.together}>

      </section>

      <section className={css.products}>

      </section>

      <section className={css.customize}>

      </section>

      <SalesServices params="consulting integration extension administration"/>
      <Offer h1="Transform a vanilla Salesforce experience into a competitive edge through ForceComprehensive’s Salesforce customization."
             a="Get a quote"
             link="null"/>
    </Layout>
  )
}