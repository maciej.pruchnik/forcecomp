import React from "react";
import Link from "next/link";
import css from "../components/all.module.css";

export default function index() {
  return(
    <div className={css.column}>
      <Link href="salesforce-administration"><a>Salesforce Administration</a></Link>
      <Link href="sales-consulting"><a>Salesforce Consulting</a></Link>
      <Link href="sales-customization"><a>Salesforce Customization</a></Link>
    </div>
  )
}