const withCSS = require('@zeit/next-css')
module.exports = withCSS({
  cssModules: true
})

module.exports = {
  exportTrailingSlash: true,
  exportPathMap: function() {
    return {
      '/': { page: '/' }
    };
  }
};